<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    protected $fillable = [
        'name','code',
    ];

    public function exams()
    {
        return $this->hasMany('App\Exam');
    }
    public function students()
    {
        return $this->hasMany('App\Student');
    }
}
