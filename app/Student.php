<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function department()
    {
        return $this->belongsTo('App\Department');
    }
    public function year()
    {
        return $this->belongsTo('App\Year');
    }
    public function semester()
    {
        return $this->belongsTo('App\Semester');
    }
    public function Section()
    {
        return $this->belongsTo('App\Section');
    }
    public function batch()
    {
        return $this->belongsTo('App\Batch');
    }
    public function results()
    {
        return $this->hasMany('App\Result');
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','code','department_id','year_id','semester_id','section_id','batch_id'
    ];
}
