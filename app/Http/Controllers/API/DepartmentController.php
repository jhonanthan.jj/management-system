<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Department;
use App\Http\Resources\DepartmentCollection;
use App\Http\Resources\DepartmentResource;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $allDepartment=Department::orderBy('id','DESC')->paginate(10);
        // return Department::latest()->paginate(10);

        return new DepartmentCollection($allDepartment);
    }

    public function search($field,$query)
    {
        return new DepartmentCollection(Department::where($field,'LIKE',"%$query%")->latest()->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|max:191',
            'code'=>'required|string|max:191|unique:departments',

        ]);

        return Department::create([
            'name'=>$request['name'],
            'code'=>$request['code'],
            
           

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $department = Department::findOrFail($id);

        $this->validate($request,[
            'name'=>'required|string|max:191',
            'code'=>'required|string|max:191|unique:departments,code,'.$id,

        ]);

        $department->update($request->all());
        return ['message' => 'Updated the department info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department = Department::findOrFail($id);
        // delete the user

        $department->delete();

        return ['message' => 'Department Deleted'];
    }
}
