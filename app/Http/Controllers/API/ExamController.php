<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subject;
use App\Department;
use App\Room;
use App\Semester;
use App\Section;
use App\Year;
use App\Exam;
use App\Http\Resources\ExamCollection;
use App\Http\Resources\ExamResource;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ExamData=Exam::orderBy('id','DESC')->paginate(10);
        // $ExamData->load('department');
        // $ExamData->load('room');
        // $ExamData->load('subject');
        // $ExamData->load('section');
        // $ExamData->load('semester');
        // $ExamData->load('year');
      
        return new ExamCollection($ExamData);
    }

    public function search($query_field,$query)
    {   if($query_field=='department'){
            $dept = Exam::with('department')->whereHas('department', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new ExamCollection($dept);
        }
        elseif($query_field=='room'){
            $room = Exam::with('room')->whereHas('room', function($q) use ($query){
            $q->where('code','LIKE',"%$query%");
            })->get();
            return new ExamCollection($room);
        }
        elseif($query_field=='subject'){
            $subject = Exam::with('subject')->whereHas('subject', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new ExamCollection($subject);
        }
        elseif($query_field=='section'){
            $section = Exam::with('section')->whereHas('section', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new ExamCollection($section);
        }
        elseif($query_field=='semester'){
            $semester = Exam::with('semester')->whereHas('semester', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new ExamCollection($semester);
        }
        elseif($query_field=='year'){
            $year = Exam::with('year')->whereHas('year', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new ExamCollection($year);
        }
        else {
            return new ExamCollection(Exam::where($query_field,'LIKE',"%$query%")->latest()->paginate(10));
        } 
       
    //    $data= DB::table('subjects')->join('departments','departments.id','subjects.department_id')->where('departments.name','LIKE',"%$query%")->get();
       
    //    return $data;
        
        // return new SubjectCollection(Subject::where($field,'LIKE',"%$query%")->latest()->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|max:191',
            'code'=>'required|string|max:191|unique:exams',

        ]);

        return Exam::create([
            'name'=>$request['name'],
            'code'=>$request['code'],
            'date'=>$request['date'],
            'department_id'=>$request['department_id'],
            'room_id'=>$request['room_id'],
            'subject_id'=>$request['subject_id'],
            'section_id'=>$request['section_id'],
            'semester_id'=>$request['semester_id'],
            'year_id'=>$request['year_id'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $exam = Exam::findOrFail($id);

        $this->validate($request,[
            'name'=>'required|string|max:191',
            'code'=>'required|string|max:191|unique:exams,code,'.$id,

        ]);

        $exam->update($request->all());
        return ['message' => 'Updated the exam info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $exam = Exam::findOrFail($id);
        // delete the user

        $exam->delete();

        return ['message' => 'Exam Deleted'];
    }
}
