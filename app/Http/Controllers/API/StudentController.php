<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use App\Department;
use App\Semester;
use App\Section;
use App\Batch;
use App\Year;
use App\Http\Resources\StudentCollection;
use App\Http\Resources\StudentResource;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allStudent=Student::orderBy('id','DESC')->paginate(10);
        $allStudent->load('department');
        $allStudent->load('year');
        $allStudent->load('semester');
        $allStudent->load('section');
        $allStudent->load('batch');
        return  new StudentCollection($allStudent);
    }

    public function search($query_field,$query)
    {
        if($query_field=='department'){
            $dept = Student::with('department')->whereHas('department', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new StudentCollection($dept);
        }
        elseif($query_field=='year'){
            $year = Student::with('year')->whereHas('year', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new StudentCollection($year);
        }
        elseif($query_field=='semester'){
            $semester = Student::with('semester')->whereHas('semester', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new StudentCollection($semester);
        }
        elseif($query_field=='section'){
            $section = Student::with('section')->whereHas('section', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new StudentCollection($section);
        }
        elseif($query_field=='batch'){
            $batch = Student::with('batch')->whereHas('batch', function($q) use ($query){
            $q->where('name','LIKE',"%$query%");
            })->get();
            return new StudentCollection($batch);
        }
        else {
            return new StudentCollection(Student::where($query_field,'LIKE',"%$query%")->latest()->paginate(10));
        } 
        // return new StudentCollection(Student::where($field,'LIKE',"%$query%")->latest()->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|max:191',
            'code'=>'required|string|max:191|unique:students',
        ]);

        return Student::create([
            'name'=>$request['name'],
            'code'=>$request['code'],
            'department_id'=>$request['department_id'],
            'year_id'=>$request['year_id'],
            'semester_id'=>$request['semester_id'],
            'section_id'=>$request['section_id'],
            'batch_id'=>$request['batch_id'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = Student::findOrFail($id);

        $this->validate($request,[
            'name'=>'required|string|max:191',
            'code'=>'required|string|max:191|unique:students,code,'.$id,

        ]);

        $student->update($request->all());
        return ['message' => 'Updated the student info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::findOrFail($id);
        // delete the user

        $student->delete();

        return ['message' => 'Student Deleted'];
    }
}
