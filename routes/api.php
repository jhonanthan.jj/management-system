<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::apiResources(['user'=>'API\UserController']);
Route::apiResources(
                        ['user'=>'API\UserController',
                        'student'=>'API\StudentController',
                        'subject'=>'API\SubjectController',
                        'department'=>'API\DepartmentController',
                        'semester'=>'API\SemesterController',
                        'section'=>'API\SectionController',
                        'room'=>'API\RoomController',
                        'batch'=>'API\BatchController',
                        'year'=>'API\YearController',
                        'exam'=>'API\ExamController',
                        'result'=>'API\ResultController'
                        ]
                    );
Route::get('search/department/{field}/{query}','API\DepartmentController@search');
Route::get('search/subject/{query_field}/{query}','API\SubjectController@search');
Route::get('search/student/{field}/{query}','API\StudentController@search');
Route::get('search/batch/{field}/{query}','API\BatchController@search');
Route::get('search/room/{field}/{query}','API\RoomController@search');
Route::get('search/section/{field}/{query}','API\SectionController@search');
Route::get('search/semester/{field}/{query}','API\SemesterController@search');
Route::get('search/year/{field}/{query}','API\YearController@search');
Route::get('search/exam/{field}/{query}','API\ExamController@search');
Route::get('search/result/{query_field}/{query}','API\ResultController@search');