<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\Department::class, 12)->create();
        factory(App\Subject::class, 19)->create();
        factory(App\Semester::class, 3)->create();
        factory(App\Section::class, 3)->create();
        factory(App\Room::class, 9)->create();
        factory(App\Batch::class, 5)->create();
        factory(App\Year::class, 5)->create();
    }
}
