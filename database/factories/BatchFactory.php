<?php

use Faker\Generator as Faker;

$factory->define(App\Batch::class, function (Faker $faker) {
    return [
        'name'=>$faker->unique()->randomElement([
            'loopers',
            'bits',
            'GHz',
            'Oprotiroddho',
            'nesting'
            ]),
        'code'=>$faker->randomElement([
            '33',
            '34',
            '28',
            '30',
            '29'
            ]),
    ];
});
