<?php

use Faker\Generator as Faker;

$factory->define(App\Semester::class, function (Faker $faker) {
    return [
        'name'=>$faker->unique()->randomElement([
            'Spring',
            'Fall',
            'Summer'
            ]),
        'code'=>$faker->unique()->randomElement([
            '01',
            '02',
            '03'
            ]),
    ];
});
