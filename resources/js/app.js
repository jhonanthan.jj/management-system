
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');



window.Vue = require('vue');
import { Form, HasError, AlertError } from 'vform'

import swal from 'sweetalert2'
window.swal=swal;

 
const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

window.toast=toast;


Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)



window.Form = Form;

import VueRouter from 'vue-router'
Vue.use(VueRouter)


import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '3px'
  })


let routes = [
    { path: '/dashboard', component:  require('./components/Dashboard.vue') },
    { path: '/profile', component:  require('./components/Profile.vue') },
    { path: '/users', component:  require('./components/Users.vue') },
    { path: '/years', component:  require('./components/Years.vue') },
    { path: '/subjects', component:  require('./components/Subjects.vue') },
    { path: '/rooms', component:  require('./components/Rooms.vue') },
    { path: '/holidays', component:  require('./components/Holidays.vue') },
    { path: '/offdays', component:  require('./components/Offdays.vue') },
    { path: '/departments', component:  require('./components/Departments.vue') },
    { path: '/sections', component:  require('./components/Sections.vue') },
    { path: '/semesters', component:  require('./components/Semesters.vue') },
    { path: '/batches', component:  require('./components/Batches.vue') },
    { path: '/students', component:  require('./components/Students.vue') },
    { path: '/exams', component:  require('./components/Exams.vue') },
    { path: '/results', component:  require('./components/Results.vue') }
  ]

const router = new VueRouter(
  {
    mode:'history',
    routes
  }
)


window.Fire =  new Vue();
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('pagination', require('./components/partial/PaginationComponent.vue'));


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
    
const app = new Vue({
    el: '#app',
    router
});
