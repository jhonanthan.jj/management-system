
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Management System</title>

<link rel="stylesheet" href="/css/app.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>

    </ul>

    <!-- SEARCH FORM -->
    {{-- <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form> --}}

    <!-- Right navbar links -->

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="./images/shop.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Laravue</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="./images/profile.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">
          {{ Auth::user()->name }}

          </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item">
                <router-link to="/dashboard" class="nav-link">
                  <i class="fas fa-tachometer-alt blue" ></i>
                  <p>Dashboard</p>
                </a>
              </li>
              <li class="nav-item has-treeview menu-open">
                <a href="#" class="nav-link">
                    <i class="fas fa-university red"></i>
                  <p>
                    Academic
                    <i class="right fa fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview"> 
                  <li class="nav-item">
                    <router-link to="/students" class="nav-link">
                      <i class="fas fa-user-graduate orange"></i>
                      <p>Student</p>
                    </router-link>
                  </li>
                  <li class="nav-item">
                    <router-link to="/results" class="nav-link">
                        <i class="fas fa-users nav-icon"></i>
                      <p>Result</p>
                    </router-link>
                  </li>
                  <li class="nav-item">
                    <router-link to="/years" class="nav-link">
                      <i class="fas fa-calendar-alt yellow"></i>
                      <p>Academic Year</p>
                    </router-link>
                  </li>
                  <li class="nav-item">
                    <router-link to="/subjects" class="nav-link">
                      <i class="fas fa-book-reader green"></i>
                      <p>Subject</p>
                    </router-link>
                  </li>
                  <li class="nav-item">
                    <router-link to="/rooms" class="nav-link">
                      <i class="fas fa-warehouse cyan"></i>
                      <p>Room</p>
                    </router-link>
                  </li>
                  <li class="nav-item">
                    <router-link to="/holidays" class="nav-link">
                        <i class="fas fa-users nav-icon"></i>
                      <p>Holiday</p>
                    </router-link>
                  </li>
                  <li class="nav-item">
                    <router-link to="/offdays" class="nav-link">
                        <i class="fas fa-users nav-icon"></i>
                      <p>Offday</p>
                    </router-link>
                  </li>
                  <li class="nav-item">
                    <router-link to="/departments" class="nav-link">
                        <i class="fas fa-users nav-icon"></i>
                      <p>Department</p>
                    </router-link>
                  </li>
                  <li class="nav-item">
                    <router-link to="/sections" class="nav-link">
                        <i class="fas fa-users nav-icon"></i>
                      <p>Section</p>
                    </router-link>
                  </li>
                  <li class="nav-item">
                    <router-link to="/semesters" class="nav-link">
                        <i class="fas fa-users nav-icon"></i>
                      <p>Semester</p>
                    </router-link>
                  </li>
                  <li class="nav-item">
                    <router-link to="/batches" class="nav-link">
                        <i class="fas fa-users nav-icon"></i>
                      <p>Batch</p>
                    </router-link>
                  </li>
                  <li class="nav-item">
                    <router-link to="/exams" class="nav-link">
                        <i class="fas fa-users nav-icon"></i>
                      <p>Examination</p>
                    </router-link>
                  </li>
                </ul>
              </li>
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link">
                <i class="fas fa-tachometer-alt"></i>
              <p>
                Starter Pages
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="fas fa-cart-plus"></i>
                  <p>Active Page</p>
                </a>
              </li>
              <li class="nav-item">
                <router-link to="/users" class="nav-link">
                    <i class="fas fa-users nav-icon"></i>
                  <p>Users</p>
                </router-link>
              </li>
            </ul>
          </li>
          <li class="nav-item">
         
            <a class="nav-link" href="{{ route('logout') }}"
          
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                           <i class="fas fa-sign-out-alt red"></i>
                          <p>
                            {{ __('Logout') }}

                          </p>
            
         </a>

         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
             @csrf
         </form>
            
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
   
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
            <router-view></router-view>
            <vue-progress-bar></vue-progress-bar>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2018 <a href="https://www.facebook.com/redxsolutions/">RedX Solutions</a>.</strong> All rights reserved.
  </footer>
</div>

<script src="/js/app.js"></script>


</body>
</html>
